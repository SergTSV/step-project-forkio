# STEP PROJECT FORKIO

## Created by Sergii Tovstuha & Artem Shchelinskyi


### Technologies:


* **HTML, CSS, JS;**
* **GIT;**
* **SCSS;**
* **Gulp;**
* **Plugins: "browser-sync","del", "fs", "gulp", "gulp-autoprefixer", "gulp-clean-css", "gulp-file-include", "gulp-fonter", "gulp-group-css-media-queries", "gulp-if", "gulp-imagemin", "gulp-newer", "gulp-notify", "gulp-plumber", "gulp-purifycss", "gulp-rename", "gulp-replace", "gulp-sass", "gulp-svg-sprite", "gulp-ttf2woff2", "gulp-util",
"gulp-version-number", "gulp-webp", "gulp-webp-html-nosvg", "gulp-webpcss", "gulp-zip", "sass", "swiper", "vinyl-ftp", "webp-converter", "webpack","webpack-stream".**


## Tasks performed by the participants:
**Worked together on: project structure, gulp settins, readme.md.**


- **Sergii Tovstuha**

  Layout of the following sections:
  - Site header with a top menu (including a drop-down menu at low screen resolutions);
  - "People Are Talking About Fork".
    
  Development and connection of media queries of the above sections.


- **Artem Shchelinskyi**

  Layout of the following sections:
  - "Revolutionary Editor";
  - "Here is what you get";
  - "Fork Subscription Pricing".

  Development and connection of media queries of the above sections.

### Start local server

```sh
npm run dev
```

### Create production build

```sh
npm run build
```


### Page link

https://SergTSV.gitlab.io/step-project-forkio



