const hamMenu = document.querySelector('.ham-menu');
const hamMenuContent = document.querySelector('.ham-menu-content');

const toggleMenu = () => {
    hamMenu.classList.toggle('active');
    hamMenuContent.classList.toggle("show");
}

window.addEventListener('resize', () => {
    hamMenu.classList.remove('active');
    hamMenuContent.classList.remove("show");
});


hamMenu.addEventListener('click', (e) => {
    e.stopPropagation();
    toggleMenu();
})

document.addEventListener('click', (e) => {
    const target = e.target;
    const its_menu = target === hamMenuContent || hamMenuContent.contains(target);
    const its_burger = target === hamMenu;
    const menu_is_active = hamMenuContent.classList.contains("show");

    if (!its_menu && !its_burger && menu_is_active ) {
        toggleMenu();
    }
})